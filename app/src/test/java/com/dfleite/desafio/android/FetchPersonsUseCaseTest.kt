package com.dfleite.desafio.android

import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCase
import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCaseImpl
import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput
import com.dfleite.desafio.android.github.persons.domain.repository.PersonsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.HttpURLConnection

@ExperimentalCoroutinesApi
class FetchPersonsUseCaseTest {

    @Mock
    private lateinit var mPersonsRepository: PersonsRepository

    private val mFetchPersonsUseCase: FetchPersonsUseCase by lazy {
        FetchPersonsUseCaseImpl(mPersonsRepository)
    }

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `should fetch person useCase Succeed`() = runBlockingTest {
        val expecResult = emptyList<GitViewDataOutput>()

        whenever(mPersonsRepository.getPersons("1")).thenReturn(expecResult)

        val result = mFetchPersonsUseCase.fetchPerson("1")

        verify(mPersonsRepository, times(1)).getPersons("1")

        assertEquals(expecResult, result)
    }

    @Test(expected = HttpException::class)
    fun `should fetch person useCase Fail`() = runBlockingTest {
        val expecResult = HttpException(Response.error<HttpException>(HttpURLConnection.HTTP_NOT_FOUND,"".toResponseBody()))

        whenever(mPersonsRepository.getPersons("1")).thenThrow(expecResult)

         mFetchPersonsUseCase.fetchPerson("1")

        verify(mPersonsRepository, times(1)).getPersons("1")

    }

}
