package com.dfleite.desafio.android.github.persons.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.dfleite.desafio.android.databinding.ActivityMainBinding
import com.dfleite.desafio.android.github.persons.ui.adapter.GitListAdapter
import com.dfleite.desafio.android.github.persons.ui.viewmodel.MainViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mViewModel: MainViewModel by viewModel()
    private val mGitListAdapter: GitListAdapter by lazy {
        GitListAdapter()
    }
    private val mViewBiding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBiding.root)
        observer()
        adapter()

    }


    private fun observer() {
        lifecycleScope.launch {
            mViewModel.getPersons().collectLatest {
                mViewBiding.recyclerView.visibility = View.VISIBLE
                mGitListAdapter.submitData(it)
                done()
            }
        }
    }

    private fun adapter() {
        mViewBiding.recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
        mViewBiding.recyclerView.adapter = mGitListAdapter
    }

    private fun done() {
        mViewBiding.userListProgressBar.visibility = View.GONE
    }


}
