package com.dfleite.desafio.android.github.persons.data.extensions

import com.dfleite.desafio.android.github.persons.data.datasource.remote.model.GitRepositoryResponse
import com.dfleite.desafio.android.github.persons.data.datasource.remote.model.Owner
import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput


fun GitRepositoryResponse.toGitViewDataOutput(): List<GitViewDataOutput> {
    return this.items.map { git ->
        val owner: Owner = git.owner

        GitViewDataOutput(
            owner.avatarUrl ,
            git.name ,
            git.id ,
            owner.login ,
            git.forksCount ,
            git.stargazersCount
        )
    }
}


