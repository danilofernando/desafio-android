package com.dfleite.desafio.android

import android.app.Application
import com.dfleite.desafio.android.common.di.networkingModule
import com.dfleite.desafio.android.github.persons.di.gitFeatureModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startModules()
    }

    private fun startModules() {
        startKoin {
            androidContext(this@MyApplication)
            modules(networkingModule, gitFeatureModule)
        }
    }
}
