package com.dfleite.desafio.android.github.persons.data.datasource.remote

import com.dfleite.desafio.android.github.persons.data.datasource.remote.model.GitRepositoryResponse

interface GitRemote {
    suspend fun fetchRepository(page: String): GitRepositoryResponse
}
