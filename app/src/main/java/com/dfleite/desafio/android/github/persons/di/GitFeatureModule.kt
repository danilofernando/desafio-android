package com.dfleite.desafio.android.github.persons.di

import com.dfleite.desafio.android.common.di.RETROFIT
import com.dfleite.desafio.android.github.persons.data.datasource.remote.GitRemote
import com.dfleite.desafio.android.github.persons.data.datasource.remote.impl.GitRemoteImpl
import com.dfleite.desafio.android.github.persons.data.datasource.remote.service.GitService
import com.dfleite.desafio.android.github.persons.data.repository.PersonsRepositoryImpl
import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCase
import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCaseImpl
import com.dfleite.desafio.android.github.persons.domain.repository.PersonsRepository
import com.dfleite.desafio.android.github.persons.ui.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object Constants {
    const val mGetPersonsUC = "getPersonsUC"
}

val gitFeatureModule = module {
    single<GitService> {
        get<Retrofit>(named(RETROFIT)).create(GitService::class.java)
    }
    single<GitRemote> { GitRemoteImpl(gitService = get()) }
    single<PersonsRepository> { PersonsRepositoryImpl(mGitRemote = get()) }
    factory<FetchPersonsUseCase>(named(Constants.mGetPersonsUC)) {
        FetchPersonsUseCaseImpl(
            mPersonsRepository = get()
        )
    }

    viewModel { MainViewModel(mFetchPersonsUseCase = get(named(Constants.mGetPersonsUC))) }
}
