package com.dfleite.desafio.android.github.persons.domain


import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput
import com.dfleite.desafio.android.github.persons.domain.repository.PersonsRepository

interface FetchPersonsUseCase{
    suspend fun fetchPerson(page:String): List<GitViewDataOutput>
}

class FetchPersonsUseCaseImpl(
    private val mPersonsRepository: PersonsRepository
) : FetchPersonsUseCase {

    override suspend fun fetchPerson(page: String): List<GitViewDataOutput> {
        return mPersonsRepository.getPersons(page)
    }
}
