package com.dfleite.desafio.android.github.persons.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData

class GitListDiffCallback : DiffUtil.ItemCallback<GitViewData>() {

    override fun areItemsTheSame(oldItem: GitViewData, newItem: GitViewData): Boolean =
        oldItem.author == newItem.author

    override fun areContentsTheSame(oldItem: GitViewData, newItem: GitViewData): Boolean =
        oldItem == newItem
}
