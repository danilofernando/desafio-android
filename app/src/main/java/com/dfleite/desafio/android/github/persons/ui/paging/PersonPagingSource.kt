package com.dfleite.desafio.android.github.persons.ui.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCase
import com.dfleite.desafio.android.github.persons.ui.extensions.toGitViewData
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData
import java.lang.Exception

class PersonPagingSource(
    private val mFetchPersonsUseCase: FetchPersonsUseCase
) : PagingSource<Int, GitViewData>() {

    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }

    override fun getRefreshKey(state: PagingState<Int, GitViewData>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GitViewData> {

        val nextPage: Int = params.key ?: FIRST_PAGE_INDEX
        return try {
            val response = mFetchPersonsUseCase.fetchPerson(nextPage.toString()).toGitViewData()

            LoadResult.Page(
                data = response,
                prevKey = if (nextPage == FIRST_PAGE_INDEX) null else nextPage,
                nextKey = if (response.isEmpty()) null else nextPage + 1
            )


        } catch (e: Exception) {
            LoadResult.Error(e)
        }

    }


}