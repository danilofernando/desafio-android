package com.dfleite.desafio.android.github.persons.ui.viewmodel


import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData

import com.dfleite.desafio.android.github.persons.domain.FetchPersonsUseCase
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData
import com.dfleite.desafio.android.github.persons.ui.paging.PersonPagingSource

import kotlinx.coroutines.flow.Flow


private const val NETWORK_PAGE_SIZE = 10


class MainViewModel(
    private val mFetchPersonsUseCase: FetchPersonsUseCase,
) : ViewModel() {

    fun getPersons(): Flow<PagingData<GitViewData>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = true
            ),
            pagingSourceFactory = { PersonPagingSource(mFetchPersonsUseCase) }
        ).flow

    }

}
