package com.dfleite.desafio.android.github.persons.data.datasource.remote.model

import com.google.gson.annotations.SerializedName



data class Owner(

    @SerializedName("login") val login: String,
    @SerializedName("id") val id: Int,
    @SerializedName("avatar_url") val avatarUrl: String

)