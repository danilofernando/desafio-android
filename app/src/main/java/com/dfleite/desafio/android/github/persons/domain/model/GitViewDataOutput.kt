package com.dfleite.desafio.android.github.persons.domain.model

data class GitViewDataOutput(
    val img: String = "",
    val nameRepo: String = "",
    val id: Int = 0,
    val author: String = "",
    val fork: String = "",
    val star: String = ""
)
