package com.dfleite.desafio.android.github.persons.domain.repository

import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput

interface PersonsRepository {
    suspend fun getPersons(page: String): List<GitViewDataOutput>
}
