package com.dfleite.desafio.android.github.persons.data.datasource.remote.model

import com.google.gson.annotations.SerializedName


data class GitRepositoryResponse(
    @SerializedName("total_count") val totalCount: Int,
    @SerializedName("incomplete_results") val incompleteResults: Boolean,
    @SerializedName("items") val items: List<GitRepository>,
)


data class GitRepository(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("owner") val owner: Owner,
    @SerializedName("stargazers_count") val stargazersCount: String,
    @SerializedName("forks_count") val forksCount: String,
)

