package com.dfleite.desafio.android.github.persons.data.datasource.remote.service

import com.dfleite.desafio.android.github.persons.data.datasource.remote.model.GitRepositoryResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface GitService {

    @GET("search/repositories")
    suspend fun fetchRepository(
        @QueryMap queryMap: HashMap<String, Any>? = hashMapOf(
            Pair("q", "language:kotlin"),
            Pair("sort", "stars")
        ),
        @Query("page") page: Int
    ): GitRepositoryResponse


}
