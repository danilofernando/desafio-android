package com.dfleite.desafio.android.github.persons.ui.extensions

import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData

fun List<GitViewDataOutput>.toGitViewData(): List<GitViewData> =
    this.map {
        GitViewData(it.img, it.nameRepo, it.id, it.author, it.fork, it.star)
    }
