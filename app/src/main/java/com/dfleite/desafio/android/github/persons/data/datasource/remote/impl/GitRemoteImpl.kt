package com.dfleite.desafio.android.github.persons.data.datasource.remote.impl

import com.dfleite.desafio.android.github.persons.data.datasource.remote.GitRemote
import com.dfleite.desafio.android.github.persons.data.datasource.remote.model.GitRepositoryResponse
import com.dfleite.desafio.android.github.persons.data.datasource.remote.service.GitService

class GitRemoteImpl(
    private val gitService: GitService
) : GitRemote {
    override suspend fun fetchRepository(page: String): GitRepositoryResponse =
        gitService.fetchRepository(page = page.toInt())



}
