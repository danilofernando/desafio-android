package com.dfleite.desafio.android.github.persons.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.dfleite.desafio.android.databinding.ListItemUserBinding
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData

class GitListAdapter :
    PagingDataAdapter<GitViewData, GitListItemViewHolder>(GitListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitListItemViewHolder {
        val binding =
            ListItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return GitListItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GitListItemViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }
}
