package com.dfleite.desafio.android.github.persons.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dfleite.desafio.android.R
import com.dfleite.desafio.android.databinding.ListItemUserBinding
import com.dfleite.desafio.android.github.persons.ui.model.GitViewData
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class GitListItemViewHolder(
    private val mViewBinding: ListItemUserBinding
) : RecyclerView.ViewHolder(mViewBinding.root) {

    fun bind(gitView: GitViewData) {
        when {
            gitView.nameRepo.isNotEmpty() -> mViewBinding.nameRepo.text = gitView.nameRepo
            else -> mViewBinding.nameRepo.text = ""
        }

        when {
            gitView.author.isNotEmpty() -> mViewBinding.author.text = gitView.author
            else -> mViewBinding.author.text = ""
        }

        when {
            gitView.author.isNotEmpty() -> mViewBinding.stars.text = gitView.star
            else -> mViewBinding.stars.text = ""
        }

        when {
            gitView.author.isNotEmpty() -> mViewBinding.fork.text = gitView.fork
            else -> mViewBinding.fork.text = ""
        }

        mViewBinding.progressBar.visibility = View.VISIBLE

        when {
            gitView.img.isNotEmpty() -> {
                Picasso.get()
                    .load(gitView.img)
                    .error(R.drawable.ic_round_account_circle)
                    .into(mViewBinding.picture, object : Callback {
                        override fun onSuccess() {
                            mViewBinding.progressBar.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            mViewBinding.progressBar.visibility = View.GONE
                        }
                    })
            }
            else -> {
                mViewBinding.picture.setImageResource(R.drawable.ic_round_account_circle)
                mViewBinding.progressBar.visibility = View.GONE
            }
        }

    }
}
