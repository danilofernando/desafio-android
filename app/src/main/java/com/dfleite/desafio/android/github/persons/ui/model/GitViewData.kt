package com.dfleite.desafio.android.github.persons.ui.model

data class GitViewData(
    val img: String = "",
    val nameRepo: String = "",
    val id: Int = 0,
    val author: String = "",
    val fork: String = "",
    val star: String = ""
)
