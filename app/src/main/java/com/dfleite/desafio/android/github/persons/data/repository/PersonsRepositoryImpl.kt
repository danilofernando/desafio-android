package com.dfleite.desafio.android.github.persons.data.repository

import com.dfleite.desafio.android.github.persons.data.datasource.remote.GitRemote
import com.dfleite.desafio.android.github.persons.data.extensions.toGitViewDataOutput
import com.dfleite.desafio.android.github.persons.domain.model.GitViewDataOutput
import com.dfleite.desafio.android.github.persons.domain.repository.PersonsRepository

class PersonsRepositoryImpl(
    private val mGitRemote: GitRemote
) : PersonsRepository {
    override suspend fun getPersons(page: String): List<GitViewDataOutput> {
        return mGitRemote.fetchRepository(page = page).toGitViewDataOutput()
    }
}
