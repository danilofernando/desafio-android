Criar um app para consultar a API do Github e trazer os repositórios com mais estrelas em Kotlin, que contenha:

- Lista de Repositórios

- exemplo de chamada: https://api.github.com/search/repositories?q=language:kotlin&sort=stars&page =1

- Exibir nome do repo, quantidade de estrelas, quantidade de fork, foto e nome do autor

- Scroll infinito

- Testes unitários

Seria legal se tivesse:

- Kotlin

- Android Architecture Components

- Testes de UI usando Espresso

- Rx ou Coroutines

- Cache de imagens e da API

- Suportar mudanças de orientação das telas sem perder o estado

Avisos:

- Subir o desafio em um repositório no github/gitlab/bitbucket e mandar o link. 

- Não ter nenhuma menção do * no Repositório/Projeto.

 

Obrigado!!